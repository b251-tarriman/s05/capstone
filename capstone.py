from abc import ABC, abstractclassmethod
class Person():
    @abstractclassmethod
    def getFullName(self):
        pass
    def addRequest(self):
        pass
    def checkRequest(self):
        pass
    def addUser(self):
        pass


class Employee(Person):
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    def get_firstName(self):
        return self._firstName
    def get_lastName(self):
        return self._lastName
    def get_email(self):
        return self._email
    def get_department(self):
        return self._department

    def set_firstName(self,firstName):
        self._firstName = firstName
    def set_lastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")
    def checkRequest(self):
        pass
    def addRequest(self):
        return(f"Request has been added")
    def login(self):
        return(f"{self._email} has logged in")
    def logout(self):
        return(f"{self._email} has logged out")

members= []
class TeamLead(Person):
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    def get_firstName(self):
        return self._firstName
    def get_lastName(self):
        return self._lastName
    def get_email(self):
        return self._email
    def get_department(self):
        return self._department

    def set_firstName(self,firstName):
        self._firstName = firstName
    def set_lastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department
    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")
    def addMember(self,name):
        members.append(name)
    def get_members(self):
        return(members)
    def login(self):
        return(f"{self._email} has logged in")
    def logout(self):
        return(f"{self._email} has logged out")
    def checkRequest(self):
        pass

class Admin(Person):
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    def get_firstName(self):
        return self._firstName
    def get_lastName(self):
        return self._lastName
    def get_email(self):
        return self._email
    def get_department(self):
        return self._department

    def set_firstName(self,firstName):
        self._firstName = firstName
    def set_lastName(self,lastName):
        self._lastName = lastName
    def set_email(self,email):
        self._email = email
    def set_department(self,department):
        self._department = department
    def getFullName(self):
        return(f"{self._firstName} {self._lastName}")
    def addUser(self):
        print(f"User has been added")
    def login(self):
        print(f"{self._email} has logged in")
    def logout(self):
        print(f"{self._email} has logged out")

class Request():
    def __init__(self,name,requester,dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = "open"
    def getName(self):
        return self.name
    def getRequester(self):
        return self.requester
    def getDateRequested(self):
        return self.dateRequested
    def getStatus(self):
        return self.status

    def setName(self,name):
        self.name = name
    def setRequester(self,requester):
        self.requester = requester
    def setDateRequested(self,dateRequested):
        self.dateRequested = dateRequested
    def setStatus(self,status):
        self.status = status

    def updateRequest(self):
        print("Request Updated")
    def closeRequest(self):
        return(f"Request {self.name} has been closed")
    def cancelRequest(self):
        print("Request Cancelled")

employee1 = Employee("John","Doe","djohn@mail.com","Marketing")
employee2 = Employee("Jane","Smith","sjane@mail.com","Marketing")
employee3 = Employee("Robert","Patterson","probert@mail.com","Sales")
employee4 = Employee("Brandon","Smith","sbrandon@mail.com","Sales")
admin1 = Admin("Monika","Justin","jmonika@mail.com","Marketing")
teamLead1 = TeamLead("Michael","Specter","smichael@mail.com","Sales")
req1 = Request("New hire orientation",teamLead1,"27-Jul-2021")
req2 = Request("Laptop Repair",employee1,"1-Jul-2021")

assert employee1.getFullName() == "John Doe","Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin","Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter","Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

req2.setStatus("closed")
print(req2.closeRequest())
